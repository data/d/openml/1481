# OpenML dataset: kr-vs-k

https://www.openml.org/d/1481

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: KEEL 
**Please cite**:   

Abstract:

A chess endgame data set representing the positions on the board of the white king, the white rook, and the black king. The task is to determine the optimum number of turn required for white to win the game, which can be a draw if it takes more than sixteen turns.

Attributes Details:


1. White_king_col {a, b, c, d, e, f, g, h}
2. White_king_row {1, 2, 3, 4, 5, 6, 7, 8}
3. White_rook_col {a, b, c, d, e, f, g, h}
4. White_rook_row {1, 2, 3, 4, 5, 6, 7, 8}
5. Black_king_col {a, b, c, d, e, f, g, h}
6. Black_king_row {1, 2, 3, 4, 5, 6, 7, 8}
7. Class - Game {draw, zero, one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen, sixteen}

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1481) of an [OpenML dataset](https://www.openml.org/d/1481). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1481/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1481/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1481/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

